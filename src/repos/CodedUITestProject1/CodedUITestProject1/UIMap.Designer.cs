﻿// ------------------------------------------------------------------------------
//  <auto-generated>
//      Этот код был создан построителем кодированных тестов ИП.
//      Версия: 16.0.0.0
//
//      Изменения, внесенные в этот файл, могут привести к неправильной работе кода и будут
//      утрачены при повторном формировании кода.
//  </auto-generated>
// ------------------------------------------------------------------------------

namespace CodedUITestProject1
{
    using System;
    using System.CodeDom.Compiler;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITest.Input;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UITesting.WindowsRuntimeControls;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = Microsoft.VisualStudio.TestTools.UITest.Input.MouseButtons;
    
    
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public partial class UIMap
    {
        
        /// <summary>
        /// ValidateSum - Используйте "ValidateSumExpectedValues" для передачи параметров в этот метод.
        /// </summary>
        public void ValidateSum()
        {
            #region Variable Declarations
            XamlText uIОтображатькак0Text = this.UIКалькуляторWindow.UIItemGroup.UIОтображатькак0Text;
            #endregion

            // Убедитесь, что свойство "ControlType" "Отображать как 0" надпись равно "3"
            Assert.AreEqual(this.ValidateSumExpectedValues.UIОтображатькак0TextControlType, uIОтображатькак0Text.ControlType.ToString());
        }
        
        #region Properties
        public virtual ValidateSumExpectedValues ValidateSumExpectedValues
        {
            get
            {
                if ((this.mValidateSumExpectedValues == null))
                {
                    this.mValidateSumExpectedValues = new ValidateSumExpectedValues();
                }
                return this.mValidateSumExpectedValues;
            }
        }
        
        public UIКалькуляторWindow UIКалькуляторWindow
        {
            get
            {
                if ((this.mUIКалькуляторWindow == null))
                {
                    this.mUIКалькуляторWindow = new UIКалькуляторWindow();
                }
                return this.mUIКалькуляторWindow;
            }
        }
        #endregion
        
        #region Fields
        private ValidateSumExpectedValues mValidateSumExpectedValues;
        
        private UIКалькуляторWindow mUIКалькуляторWindow;
        #endregion
    }
    
    /// <summary>
    /// Параметры для передачи в "ValidateSum"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class ValidateSumExpectedValues
    {
        
        #region Fields
        /// <summary>
        /// Убедитесь, что свойство "ControlType" "Отображать как 0" надпись равно "3"
        /// </summary>
        public string UIОтображатькак0TextControlType = "3";
        #endregion
    }
    
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class UIКалькуляторWindow : XamlWindow
    {
        
        public UIКалькуляторWindow()
        {
            #region Условия поиска
            this.SearchProperties[XamlControl.PropertyNames.Name] = "Калькулятор";
            this.SearchProperties[XamlControl.PropertyNames.ClassName] = "Windows.UI.Core.CoreWindow";
            this.WindowTitles.Add("Калькулятор");
            #endregion
        }
        
        #region Properties
        public UIItemGroup UIItemGroup
        {
            get
            {
                if ((this.mUIItemGroup == null))
                {
                    this.mUIItemGroup = new UIItemGroup(this);
                }
                return this.mUIItemGroup;
            }
        }
        #endregion
        
        #region Fields
        private UIItemGroup mUIItemGroup;
        #endregion
    }
    
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class UIItemGroup : XamlControl
    {
        
        public UIItemGroup(XamlControl searchLimitContainer) : 
                base(searchLimitContainer)
        {
            #region Условия поиска
            this.SearchProperties[UITestControl.PropertyNames.ControlType] = "Group";
            this.WindowTitles.Add("Калькулятор");
            #endregion
        }
        
        #region Properties
        public XamlText UIОтображатькак0Text
        {
            get
            {
                if ((this.mUIОтображатькак0Text == null))
                {
                    this.mUIОтображатькак0Text = new XamlText(this);
                    #region Условия поиска
                    this.mUIОтображатькак0Text.SearchProperties[XamlText.PropertyNames.AutomationId] = "CalculatorResults";
                    this.mUIОтображатькак0Text.WindowTitles.Add("Калькулятор");
                    #endregion
                }
                return this.mUIОтображатькак0Text;
            }
        }
        #endregion
        
        #region Fields
        private XamlText mUIОтображатькак0Text;
        #endregion
    }
}
