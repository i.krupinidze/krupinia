﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using BankDB_Krupin;

namespace MyBank_Tests_KRUPIN
{
    [TestClass]
    public class KRUPIN_TESTS
    {
        public TestContext TestContext { get; set; }

        [TestMethod]
        [DataSource(@"Provider=Microsoft.SqlServerCe.Client.4.0;Data Source=D:\MS SQL Server 2019\MSSQL15.IVAN\MSSQL\DATA\MathsData.sdf", "AddIntegersData")]
        public void AddIntegers_FromDataSourceTest()
        {
            var target = new Maths_Krupin();

            // Access the data
            int x = Convert.ToInt32(TestContext.DataRow["FirstNumber"]);
            int y = Convert.ToInt32(TestContext.DataRow["SecondNumber"]);
            int expected = Convert.ToInt32(TestContext.DataRow["Sum"]);
            int actual = target.AddIntegers(x, y);
            Assert.AreEqual(expected, actual,
                "x:<{0}> y:<{1}>",
                new object[] { x, y });
        }
    }
}
