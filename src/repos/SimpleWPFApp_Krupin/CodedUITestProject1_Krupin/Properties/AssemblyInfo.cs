﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// Управление общими сведениями о сборке осуществляется с помощью указанного ниже 
// атрибутов. Отредактируйте значения этих атрибутов, чтобы изменить
// связанные со сборкой.
[assembly: AssemblyTitle("CodedUITestProject1_Krupin")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("CodedUITestProject1_Krupin")]
[assembly: AssemblyCopyright("Copyright ©  2022")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Задание значения false для атрибута ComVisible приведет к тому, что типы из этой сборки станут невидимыми 
// для COM-компонентов.  Если к одному из типов этой сборки необходимо обращаться из 
// COM, задайте атрибуту ComVisible значение TRUE для этого типа.
[assembly: ComVisible(false)]

// Если данный проект доступен для модели COM, следующий GUID используется в качестве идентификатора библиотеки типов
[assembly: Guid("1a74b171-dfdd-45f9-9f78-226940bedf19")]

// Сведения о версии сборки состоят из следующих четырех значений:
//
//      Основной номер версии
//      Дополнительный номер версии 
//   Номер сборки
//      Редакция
//
// Можно задать все значения или принять номера сборки и редакции по умолчанию 
// используя "*", как показано ниже:
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
