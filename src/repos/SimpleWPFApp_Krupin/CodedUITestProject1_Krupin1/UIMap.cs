﻿namespace CodedUITestProject1_Krupin1
{
    using Microsoft.VisualStudio.TestTools.UITesting.WinControls;
    using Microsoft.VisualStudio.TestTools.UITesting.WpfControls;
    using System;
    using System.Collections.Generic;
    using System.CodeDom.Compiler;
    using Microsoft.VisualStudio.TestTools.UITest.Extension;
    using Microsoft.VisualStudio.TestTools.UITesting;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;
    using Mouse = Microsoft.VisualStudio.TestTools.UITesting.Mouse;
    using MouseButtons = System.Windows.Forms.MouseButtons;
    using System.Drawing;
    using System.Windows.Input;
    using System.Text.RegularExpressions;
   


    public partial class UIMap
    {

        /// <summary>
        /// SimpleAppTest_Krupin - Используйте "SimpleAppTest_KrupinParams" для передачи параметров в этот метод.
        /// </summary>
        public void ModifiedSimpleAppTest_Krupin()
        {
            #region Variable Declarations
            WpfButton uIStartButton = this.UIMainWindowWindow.UIStartButton;
            WpfCheckBox uICheckBoxCheckBox = this.UIMainWindowWindow.UICheckBoxCheckBox;
            WinButton uIЗакрытьButton = this.UIMainWindowWindow1.UIЗакрытьButton;
            #endregion

            // Запуск "%USERPROFILE%\source\repos\SimpleWPFApp_Krupin\SimpleWPFApp_Krupin\bin\Debug\SimpleWPFApp_Krupin.exe"
            ApplicationUnderTest uIMainWindowWindow = ApplicationUnderTest.Launch(this.SimpleAppTest_KrupinParams.UIMainWindowWindowExePath, this.SimpleAppTest_KrupinParams.UIMainWindowWindowAlternateExePath);

            // Щелкните "Start" кнопка
            Mouse.Click(uIStartButton, new Point(39, 15));
            uICheckBoxCheckBox.WaitForControlEnabled();
            // Выбор "CheckBox" флажок
            uICheckBoxCheckBox.Checked = this.SimpleAppTest_KrupinParams.UICheckBoxCheckBoxChecked;

            // Щелкните "Закрыть" кнопка
            Mouse.Click(uIЗакрытьButton, new Point(30, 18));
        }
        public UIMap()
        {
            this.UIMainWindowWindow.UIStartButton.SearchProperties[WpfButton.PropertyNames.AutomationId] = "buttonA";
        }
        public virtual SimpleAppTest_KrupinParams SimpleAppTest_KrupinParams
        {
            get
            {
                if ((this.mSimpleAppTest_KrupinParams == null))
                {
                    this.mSimpleAppTest_KrupinParams = new SimpleAppTest_KrupinParams();
                }
                return this.mSimpleAppTest_KrupinParams;
            }
        }

        private SimpleAppTest_KrupinParams mSimpleAppTest_KrupinParams;
    }
    /// <summary>
    /// Параметры для передачи в "SimpleAppTest_Krupin"
    /// </summary>
    [GeneratedCode("Построитель кодированных тестов ИП", "16.0.32802.440")]
    public class SimpleAppTest_KrupinParams
    {

        #region Fields
        /// <summary>
        /// Запуск "%USERPROFILE%\source\repos\SimpleWPFApp_Krupin\SimpleWPFApp_Krupin\bin\Debug\SimpleWPFApp_Krupin.exe"
        /// </summary>
        public string UIMainWindowWindowExePath = "C:\\Users\\vanya\\source\\repos\\SimpleWPFApp_Krupin\\SimpleWPFApp_Krupin\\bin\\Debug\\Sim" +
            "pleWPFApp_Krupin.exe";

        /// <summary>
        /// Запуск "%USERPROFILE%\source\repos\SimpleWPFApp_Krupin\SimpleWPFApp_Krupin\bin\Debug\SimpleWPFApp_Krupin.exe"
        /// </summary>
        public string UIMainWindowWindowAlternateExePath = "%USERPROFILE%\\source\\repos\\SimpleWPFApp_Krupin\\SimpleWPFApp_Krupin\\bin\\Debug\\Simp" +
            "leWPFApp_Krupin.exe";

        /// <summary>
        /// Выбор "CheckBox" флажок
        /// </summary>
        public bool UICheckBoxCheckBoxChecked = true;
        #endregion
    }
}
